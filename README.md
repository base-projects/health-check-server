Health check server. Run this server to collect status info from heal check agents.

Note: This server expect redis to be installed and running at localhost:6379 without password.

###### Installation:

`sudo npm i -g bapjsc-health-check-server --unsafe-perm`

##### Run:

`health-check-server <listen port> <config directory> <show log true/false>`

##### Configuration:

Config directory contains one or many configuration files. The configuration file is in xml format follow the XSD format as in lib/src/app/assets/xsd/server-config.xsd
Using the following header for server config file. Your IDE may be smart enough to show you the auto complete guide (WebStorm for example).

```<?xml version="1.0" encoding="UTF-8" ?>
   
   <server-config xmlns="https://xsd.bap.jp/healthcheck/server"
                  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                  xsi:schemaLocation="https://xsd.bap.jp/healthcheck/server https://gitlab-new.bap.jp/template-projects/health-check-server/raw/master/lib/src/app/assets/xsd/server-config.xsd">
   ...
   </server-config>
``` 
