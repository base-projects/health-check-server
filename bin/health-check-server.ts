#!/usr/bin/env node
import "source-map-support/register";
import path = require("path");
import {HealthCheckServer} from "../lib/src/index";
import packageJson = require("../package.json");

let port = process.argv[2];
let configDirPath = process.argv[3];
let enableLog = process.argv[4];

if (["-v", "--version"].indexOf(port) >= 0) {
  console.log(packageJson.version);
  process.exit();
}

if (!port || !configDirPath) {
  console.log("Usage:");
  console.log("health-check-server <port> <path-to-config-folder> <enable log true/false>");
  console.log("health-check-server --version/-v");
  process.exit();
} else {
  HealthCheckServer.main(parseInt(port), path.isAbsolute(configDirPath) ? configDirPath : path.join(process.cwd(), configDirPath), enableLog === "true");
}