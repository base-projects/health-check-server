import {
  IResponse,
  ClaireBuilder,
  DefaultLogger,
  IEnvProvider,
  ResponseSuccess, DefaultRedisAdapter,
} from "claire-framework";

//-- import services
import {HealthCheckService} from "./app/services/HealthCheckService";
//-- import controllers
import {HealthCheckController} from "./app/controllers/HealthCheckController";
//-- import middleware
import {CORS} from "./app/middleware/CORS";
import {BodyParser} from "./app/middleware/bodyParser";

class HealthCheckServer {
  public static main(port: number, configDirPath: string, enableLog: boolean): void {

    class MyEnvProvider implements IEnvProvider {

      private env?: Map<string, string>;

      async init(): Promise<IResponse> {
        this.env = new Map<string, string>();
        this.env.set("NODE_ENV", process.env["NODE_ENV"] || "LCL");
        this.env.set("PORT", port.toString());
        this.env.set("CONFIG_PATH", configDirPath);
        this.env.set("LOG_LEVEL", enableLog ? "0" : "3");
        this.env.set("RELOAD_CONFIG_INTERVAL_MS", process.env["RELOAD_CONFIG_INTERVAL_MS"] || "30000");
        this.env.set("REDIS_SERVER", process.env["REDIS_SERVER"] || "redis://localhost:6379");
        return new ResponseSuccess();
      }

      getEnv(varName: string): string {
        let value = this.env && this.env.get(varName);
        return value || "";
      }
    }

    const claire = new ClaireBuilder(
      new MyEnvProvider(),
      new DefaultLogger("LOG_LEVEL"),
      [],
      [
        HealthCheckService,
      ],
      [
        HealthCheckController,
      ])
      .setListenPort("PORT")
      .setCacheAdapter(new DefaultRedisAdapter("REDIS_SERVER", 30000))
      .addMiddleware([
        BodyParser,
        CORS,
      ])
      .build();

    claire.start()
      .then(() => {
        console.log("Server is running...");
      })
      .catch((err) => {
        console.log("Server exits by error: ", err);
        process.exit(1);
      });
  }
}

export {HealthCheckServer};