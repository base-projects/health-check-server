import {
  DbServiceConfig, DbServiceStateData,
  FixedGroupServerConfig,
  HealthCheckAutoScaleGroupConfig,
  HealthCheckConfig,
  HealthCheckContent,
  HealthCheckFixedGroupConfig,
  ServiceCheckContent,
  ServiceCheckStatusContent,
  ServiceConfig,
  ServiceHealthCheckData,
  ServiceType
} from "../classes/healthcheck";


export const convertHealthCheckServiceDataFromData = (data: ServiceCheckContent): ServiceHealthCheckData => {

  let currentState = undefined;

  if (data.status.success && !!data.status.result) {
    let state = data.status.result;
    if (state["maxConnections"] !== undefined && state["currentConnections"] !== undefined) {
      currentState = new DbServiceStateData(state.currentConnections, state.maxConnections);
    }
  }
  return new ServiceHealthCheckData(
    data.serviceName,
    undefined,
    data.lastContact,
    !data.status.success ? data.status.result : undefined,
    currentState
  );
};

export const convertHealthCheckDataFromPost = (data: any): HealthCheckContent => {
  let services = new Array<ServiceCheckContent>();

  data["services"] && data["services"].length && data["services"].forEach((service: any) => {
    let status = new ServiceCheckStatusContent(
      service["status"]["success"],
      service["status"]["result"],
    );
    let serviceCheckContent = new ServiceCheckContent(
      service["name"],
      status,
      service["last_contact"],
    );
    services.push(serviceCheckContent);
  });

  let result = new HealthCheckContent(
    data["domain"],
    data["group"],
    data["name"],
    services,
  );

  return result as HealthCheckContent;
};

export const convertHealthCheckConfigFromXmlJSON = (data: any): Array<HealthCheckConfig> => {
  let result = new Array<HealthCheckConfig>();

  let getServiceConfigs = (serviceConfig: any): Array<ServiceConfig> => {
    let result = new Array<ServiceConfig>();
    Object.keys(ServiceType).map(k => ServiceType[k as any]).map(v => v as ServiceType).forEach((type: ServiceType) => {
      serviceConfig[type] && serviceConfig[type].length && serviceConfig[type].forEach((service: any) => {
        let serviceConfig = new ServiceConfig(service["name"][0], parseInt(service["alive-threshold-ms"][0]), service["is-local"][0] === "true");
        switch (type) {
          case ServiceType.DB_SERVICE:
            let dbServiceConfig = new DbServiceConfig(serviceConfig.serviceName, serviceConfig.aliveThreshold, serviceConfig.isLocal, parseInt(service["connection-alert-percent"][0]));
            result.push(dbServiceConfig);
            break;
          case ServiceType.HTTP_SERVICE:
            result.push(serviceConfig);
            break;
          case ServiceType.TCP_SERVICE:
            result.push(serviceConfig);
            break;
        }
      });
    });
    return result;
  };

  let fixedGroups = data["server-config"]["groups"][0]["fixed-group"];
  fixedGroups && fixedGroups.length && fixedGroups.forEach((group: any) => {
    let servers = new Array<FixedGroupServerConfig>();
    group["servers"][0]["server"].forEach((server: any) => {
      let fixedGroupServerConfig = new FixedGroupServerConfig(
        server["name"][0],
        getServiceConfigs(server["services"][0]),
      );
      servers.push(fixedGroupServerConfig);
    });

    let fixedGroupConfig = new HealthCheckFixedGroupConfig(
      servers
    );
    let config = new HealthCheckConfig(
      Date.now(),
      data["server-config"]["domain"][0],
      group["name"][0],
      fixedGroupConfig,
    );

    result.push(config);
  });

  let autoScaleGroups = data["server-config"]["groups"][0]["auto-scaling-group"];
  autoScaleGroups && autoScaleGroups.length && autoScaleGroups.forEach((group: any) => {
    let autoScaleGroupConfig = new HealthCheckAutoScaleGroupConfig(
      group["resolve-instance-name"][0] === "true",
      parseInt(group["alive-threshold-ms"][0]),
      getServiceConfigs(group["services"][0])
    );

    let config = new HealthCheckConfig(
      Date.now(),
      data["server-config"]["domain"][0],
      group["name"][0],
      autoScaleGroupConfig,
    );
    result.push(config);
  });

  return result;
};