/**
 * Health check content posted from client
 */
export class ServiceCheckStatusContent {
  success: boolean;
  result: any;

  constructor(success: boolean, result: any) {
    this.success = success;
    this.result = result;
  }
}

export class ServiceCheckContent {
  serviceName: string;
  status: ServiceCheckStatusContent;
  lastContact?: number;

  constructor(serviceName: string, status: ServiceCheckStatusContent, lastContact?: number) {
    this.serviceName = serviceName;
    this.status = status;
    this.lastContact = lastContact;
  }
}

export class HealthCheckContent {
  domain: string;
  group: string;
  serverName: string;
  services: Array<ServiceCheckContent>;

  constructor(domain: string, group: string, serverName: string, services: Array<ServiceCheckContent>) {
    this.domain = domain;
    this.group = group;
    this.serverName = serverName;
    this.services = services;
  }
}


/**
 * Server-side config
 */

export enum ServiceType {
  DB_SERVICE = "db-service",
  HTTP_SERVICE = "http-service",
  TCP_SERVICE = "tcp-service",
}

export class ServiceConfig {
  serviceName: string;
  aliveThreshold: number;
  isLocal: boolean;

  constructor(serviceName: string, aliveThresshold: number, isLocal: boolean) {
    this.serviceName = serviceName;
    this.aliveThreshold = aliveThresshold;
    this.isLocal = isLocal;
  }
}

// export class HttpServiceConfig extends ServiceConfig {
//
// }

export class DbServiceConfig extends ServiceConfig {
  connectionAlertPercent: number;

  constructor(serviceName: string, aliveThreshold: number, isLocal: boolean, connectionAlertPercent: number) {
    super(serviceName, aliveThreshold, isLocal);
    this.connectionAlertPercent = connectionAlertPercent;
  }
}

// export class TcpServiceConfig extends ServiceConfig {
//
// }

export class FixedGroupServerConfig {
  serverName: string;
  services: Array<ServiceConfig>;

  constructor(serverName: string, services: Array<ServiceConfig>) {
    this.serverName = serverName;
    this.services = services;
  }
}

export class HealthCheckFixedGroupConfig {
  servers: Array<FixedGroupServerConfig>;

  constructor(servers: Array<FixedGroupServerConfig>) {
    this.servers = servers;
  }

}

export class HealthCheckAutoScaleGroupConfig {
  resolveInstanceName: boolean;
  aliveThreshold: number;
  services: Array<ServiceConfig>;

  constructor(resolveInstanceName: boolean, aliveThreshold: number, services: Array<ServiceConfig>) {
    this.resolveInstanceName = resolveInstanceName;
    this.aliveThreshold = aliveThreshold;
    this.services = services;
  }
}

export class HealthCheckConfig {
  lastModified: number;
  domain: string;
  group: string;
  config: HealthCheckFixedGroupConfig | HealthCheckAutoScaleGroupConfig;

  constructor(lastModified: number, domain: string, group: string, config: HealthCheckFixedGroupConfig | HealthCheckAutoScaleGroupConfig) {
    this.lastModified = lastModified;
    this.domain = domain;
    this.group = group;
    this.config = config;
  }

}

/**
 * Server-side data
 */

export class DbServiceStateData {
  currentConnections: number;
  maxConnections: number;

  constructor(currentConnection: number, maxConnection: number) {
    this.currentConnections = currentConnection;
    this.maxConnections = maxConnection;
  }
}

export class ServiceHealthCheckData {
  serviceName: string;
  isAlive?: boolean;
  lastContact?: number;
  lastProblem?: any;
  currentState?: DbServiceStateData;

  constructor(serviceName: string, isAlive: boolean | undefined, lastContact?: number, lastProblem?: any, currentState?: DbServiceStateData) {
    this.serviceName = serviceName;
    this.isAlive = isAlive;
    this.lastContact = lastContact;
    this.lastProblem = lastProblem;
    this.currentState = currentState;
  }
}

export class HealthCheckData {
  lastModified: number;
  domain: string;
  group: string;
  serverName: string;
  services: Array<ServiceHealthCheckData>;

  constructor(lastModified: number, domain: string, group: string, serverName: string, services: Array<ServiceHealthCheckData>) {
    this.lastModified = lastModified;
    this.domain = domain;
    this.group = group;
    this.serverName = serverName;
    this.services = services;
  }

}