export const setLoop = (fn: Function, timeoutMs: number, failureContinue: boolean, ...fnArgs: any[]) => {
  fn(...fnArgs)
    .then(() => {
      setTimeout(setLoop, timeoutMs, fn, timeoutMs, failureContinue, ...fnArgs);
    })
    .catch((err: any) => {
      if (failureContinue) setTimeout(setLoop, timeoutMs, fn, timeoutMs, failureContinue, ...fnArgs);
    });
};

