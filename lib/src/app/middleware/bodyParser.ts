import {IAppContext, IMiddleware, IResponse, ResponseSuccess} from "claire-framework";
import bodyParser = require("body-parser");

export class BodyParser implements IMiddleware{

  async init(appContext: IAppContext): Promise<IResponse> {
    return new ResponseSuccess();
  }

  process(): (request: any, response: any, next: any) => any {
    return bodyParser.json({});
  }
}