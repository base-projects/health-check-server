import {IMiddleware, IAppContext, IResponse, ResponseSuccess} from "claire-framework";

export class CORS implements IMiddleware {

  async init(appContext: IAppContext): Promise<IResponse> {
    return new ResponseSuccess();
  }

  process(): (request: any, response: any, next: any) => any {
    return (request: any, response: any, next: any) => {
      response.header("Access-Control-Allow-Origin", "*");
      response.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
      next();
    };
  }
}