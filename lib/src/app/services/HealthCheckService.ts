import fs = require("fs");
import path = require("path");
import xsd = require("libxmljs");
import {parseString} from "xml2js";
import {ICacheAdapter, ILogger, IResponse, IService, LL, ResponseFailure, ResponseSuccess} from "claire-framework";
import {IAppContext} from "claire-framework/dist/system/IAppContext";
import {
  DbServiceConfig, DbServiceStateData,
  HealthCheckAutoScaleGroupConfig,
  HealthCheckConfig,
  HealthCheckContent,
  HealthCheckData, HealthCheckFixedGroupConfig, ServiceConfig, ServiceHealthCheckData,
} from "../types/classes/healthcheck";
import {setLoop} from "../utils/process";
import {Errors} from "../Errors";
import {
  convertHealthCheckConfigFromXmlJSON,
  convertHealthCheckServiceDataFromData
} from "../types/converters/healthcheck";

export class HealthCheckService implements IService {

  public healthCheckData: Array<HealthCheckData> = [];
  public healthCheckConfig: Array<HealthCheckConfig> = [];
  public serverConfigSchema: any;
  public RELOAD_CONFIG_INTERVAL_MS: number = 0;

  public logger?: ILogger;
  public cacheAdapter?: ICacheAdapter;

  async init(appContext: IAppContext): Promise<IResponse> {
    const envProvider = appContext.getEnvProvider();
    const logger = appContext.getLogger();

    this.cacheAdapter = appContext.getCacheAdapter();
    this.logger = logger;
    this.RELOAD_CONFIG_INTERVAL_MS = parseInt(envProvider.getEnv("RELOAD_CONFIG_INTERVAL_MS"));

    //-- read config schema
    const xmlSchemaContent = await new Promise<IResponse>(resolve => {
      fs.readFile(path.join(__dirname, "..", "assets", "xsd", "server-config.xsd"), (err, data) => {
        if (err) {
          logger.write(LL.ERROR, err);
          return resolve(new ResponseFailure(Errors.READ_DIR_ERROR));
        }
        return resolve(new ResponseSuccess(data));
      });
    });
    if (!xmlSchemaContent.isSuccess()) {
      return xmlSchemaContent;
    }

    try {
      this.serverConfigSchema = xsd.parseXmlString(xmlSchemaContent.getResult());
    } catch (err) {
      logger.write(LL.ERROR, err);
      return new ResponseFailure(Errors.INVALID_XSD_SCHEMA);
    }

    //-- read saved data from cache
    if (this.cacheAdapter.isPersistent()) {
      this.logger && this.logger.write(LL.VERBOSE, "reading saved data from cache");
      let configData = await this.cacheAdapter.getCache("HEALTH_CHECK_CONFIG");
      if (configData.isSuccess()) {
        this.healthCheckConfig = JSON.parse(configData.getResult());
      }
      let healthCheckData = await this.cacheAdapter.getCache("HEALTH_CHECK_DATA");
      if (healthCheckData.isSuccess()) {
        this.healthCheckData = JSON.parse(healthCheckData.getResult());
      }
      this.logger && this.logger.write(LL.VERBOSE, "content loaded from cache", configData, healthCheckData);
    }

    //-- set loop for jobs
    setLoop(reloadConfigData, this.RELOAD_CONFIG_INTERVAL_MS, true, this, envProvider.getEnv("CONFIG_PATH"), logger);
    setLoop(saveHealthCheckDataToRedis, 10000, true, this);
    setLoop(checkServiceHealth, 1000, true, this);
    return new ResponseSuccess();
  }

  async getHealthCheckData(domain: string, group?: string): Promise<IResponse> {
    let result = this.healthCheckData.filter((data) => data.domain === domain && (!group || (data.group === group)));
    return new ResponseSuccess(result);
  }

  async appendHealthCheckData(data: HealthCheckContent): Promise<IResponse> {

    //-- find the corresponding config
    let config = this.healthCheckConfig.find(c => c.domain === data.domain && c.group === data.group);
    if (!config) {
      return new ResponseFailure(Errors.NO_CORRESPONDING_CONFIG);
    }

    //-- find the corresponding data
    let currentServerData = this.healthCheckData.find(d => d.domain === data.domain && d.group === data.group && d.serverName === data.serverName);
    if (!currentServerData) {
      let services = new Array<ServiceHealthCheckData>();
      data.services.forEach((s) => {
        let data = convertHealthCheckServiceDataFromData(s);
        this.logger && this.logger.write(LL.VERBOSE, "new service data after convert", data);
        services.push(data);
      });
      let healthCheckData = new HealthCheckData(
        Date.now(),
        data.domain,
        data.group,
        data.serverName,
        services
      );
      this.healthCheckData.push(healthCheckData);
    } else {
      currentServerData.lastModified = Date.now();
      let services: Array<ServiceConfig> | null;
      if (config.config instanceof HealthCheckAutoScaleGroupConfig) {
        services = config.config.services;
      } else {
        let server = config.config.servers.find(s => s.serverName === data.serverName);
        services = server ? server.services : null;
      }

      //-- update data
      if (services) {
        for (let i = 0; i < data.services.length; i++) {
          let s = data.services[i];
          //-- find corresponding config
          let correspondingConfig = services.find(s1 => s1.serviceName === s.serviceName);
          if (!correspondingConfig) {
            // -- skip, config not found
          } else if (!!currentServerData) {
            //-- find corresponding data
            let currentServiceDataIndex = currentServerData.services.findIndex(s1 => s1.serviceName === s.serviceName);
            let serviceData = convertHealthCheckServiceDataFromData(s);
            this.logger && this.logger.write(LL.VERBOSE, "actual service data after convert", serviceData);
            if (currentServiceDataIndex < 0) {
              //-- add new service data
              currentServerData.services.push(serviceData);
            } else {
              if (serviceData.isAlive === undefined) {
                serviceData.isAlive = currentServerData.services[currentServiceDataIndex].isAlive;
              }
              if (serviceData.lastContact === undefined) {
                serviceData.lastContact = currentServerData.services[currentServiceDataIndex].lastContact;
              }
              if (serviceData.lastProblem === undefined) {
                serviceData.lastProblem = currentServerData.services[currentServiceDataIndex].lastProblem;
              }
              if (serviceData.currentState === undefined) {
                serviceData.currentState = currentServerData.services[currentServiceDataIndex].currentState;
              }

              //-- update service data
              currentServerData.services = [
                ...currentServerData.services.slice(0, currentServiceDataIndex),
                serviceData,
                ...currentServerData.services.slice(currentServiceDataIndex + 1),
              ];
            }
          }
        }
      }
    }
    return new ResponseSuccess();
  }

  async stop(appContext: IAppContext): Promise<IResponse> {
    return new ResponseSuccess();
  }
}

const reloadConfigData = async (_this: HealthCheckService, configDirPath: string): Promise<IResponse> => {

  let fileResult = await new Promise<IResponse>((resolve) => {
    fs.readdir(configDirPath, (err, items) => {
      if (err) {
        _this.logger && _this.logger.write(LL.ERROR, err);
        return resolve(new ResponseFailure(Errors.READ_DIR_ERROR));
      }
      return resolve(new ResponseSuccess(items));
    });
  });
  if (!fileResult.isSuccess())
    return fileResult;

  //-- read config files and update
  const files = fileResult.getResult();
  for (let i = 0; i < files.length; i++) {
    //-- only read xml file
    let file = files[i];
    _this.logger && _this.logger.write(LL.VERBOSE, "reading config from file: " + file);
    if (file.indexOf(".xml") >= 0) {
      const configFilePath = path.join(configDirPath, file);
      const xmlConfigContent = await new Promise<IResponse>(resolve => {
        fs.readFile(configFilePath, (err, data) => {
          if (err) {
            _this.logger && _this.logger.write(LL.ERROR, err);
            return resolve(new ResponseFailure(Errors.READ_DIR_ERROR));
          }
          return resolve(new ResponseSuccess(data));
        });
      });
      if (!xmlConfigContent.isSuccess()) {
        return xmlConfigContent;
      }

      let xmlConfig: any;
      try {
        xmlConfig = xsd.parseXmlString(xmlConfigContent.getResult());
      } catch (err) {
        _this.logger && _this.logger.write(LL.ERROR, "Invalid xml format", file, err);
        continue;
      }
      const result = xmlConfig.validate(_this.serverConfigSchema);
      if (!result) {
        //-- skip this file
        _this.logger && _this.logger.write(LL.VERBOSE, "Invalid config, skipping: ", file);
        continue;
      }

      //-- parse xml config to json
      let parseConfigResult = await new Promise<IResponse>(resolve => {
        parseString(xmlConfig, (err: any, result: any) => {
          if (err) {
            _this.logger && _this.logger.write(LL.ERROR, err);
            return resolve(new ResponseFailure(Errors.XML_JSON_PARSE_ERROR));
          }
          return resolve(new ResponseSuccess(result));
        });
      });
      if (!parseConfigResult.isSuccess()) {
        _this.logger && _this.logger.write(LL.VERBOSE, "config parse not success");
        return parseConfigResult;
      }

      //-- read json and update config
      try {
        let healthCheckConfigs = convertHealthCheckConfigFromXmlJSON(parseConfigResult.getResult());
        healthCheckConfigs.forEach((config: HealthCheckConfig) => {
          config.lastModified = Date.now();
          let oldConfigIndex = _this.healthCheckConfig.findIndex((c) => c.domain === config.domain && c.group === config.group);
          if (oldConfigIndex >= 0) {
            _this.logger && _this.logger.write(LL.VERBOSE, "Replacing config at index: " + oldConfigIndex, config);
            _this.healthCheckConfig[oldConfigIndex] = config;
          } else {
            _this.logger && _this.logger.write(LL.VERBOSE, "Adding new config", config);
            _this.healthCheckConfig.push(config);
          }
        });
      } catch (err) {
        _this.logger && _this.logger.write(LL.ERROR, err);
      }
    }
  }

  //-- remove invalidated configs
  for (let i = _this.healthCheckConfig.length - 1; i >= 0; i--) {
    if (_this.healthCheckConfig[i].lastModified + _this.RELOAD_CONFIG_INTERVAL_MS < Date.now()) {
      _this.logger && _this.logger.write(LL.VERBOSE, "Removing outdated config", _this.healthCheckConfig[i]);
      _this.healthCheckConfig = [
        ..._this.healthCheckConfig.slice(0, i),
        ..._this.healthCheckConfig.slice(i + 1),
      ]
    }
  }

  _this.logger && _this.logger.write(LL.DEBUG, "current config", JSON.stringify(_this.healthCheckConfig));

  return new ResponseSuccess();
};

const checkServiceHealth = async (_this: HealthCheckService) => {

  //-- check the service threshold and update status
  for (let i = _this.healthCheckData.length - 1; i >= 0; i--) {
    let data = _this.healthCheckData[i];

    //-- find the corresponding config
    let config = _this.healthCheckConfig.find((s) => s.domain === data.domain && s.group === data.group);
    if (config) {
      if (config.config instanceof HealthCheckAutoScaleGroupConfig) {
        let autoScaleConfig = config.config;
        data.services.forEach((service) => {
          let configService = autoScaleConfig.services.find((s1) => s1.serviceName === service.serviceName);
          if (configService) {
            //-- check service aliveness
            service.isAlive = true;
            if (!!service.lastContact && (service.lastContact + (configService.aliveThreshold || autoScaleConfig.aliveThreshold) < Date.now())) {
              service.isAlive = false;
              service.lastProblem = Errors.LAST_CONTACT_THRESHOLD;
            }
            if (configService instanceof DbServiceConfig) {
              let serviceState = service.currentState as DbServiceStateData;
              if (!!serviceState.maxConnections && (configService.connectionAlertPercent < serviceState.currentConnections * 100 / serviceState.maxConnections)) {
                service.isAlive = false;
                service.lastProblem = Errors.CONNECTION_NUMBER_ALERT;
              }
            }
          }
        });
      } else {
        let fixConfig = config.config;
        let serverConfig = fixConfig.servers.find((server) => server.serverName === data.serverName);
        if (serverConfig) {
          data.services.forEach((service) => {
            // _this.logger && _this.logger.write(LL.DEBUG, "checking service", service);
            let configService = serverConfig && serverConfig.services.find((s1) => s1.serviceName === service.serviceName);
            if (configService) {
              //-- check service aliveness
              service.isAlive = true;
              if (!!service.lastContact && (service.lastContact + configService.aliveThreshold < Date.now())) {
                service.isAlive = false;
                service.lastProblem = Errors.LAST_CONTACT_THRESHOLD;
              }
              if (configService instanceof DbServiceConfig) {
                // _this.logger && _this.logger.write(LL.DEBUG, "configService is instance of DbServiceConfig");
                let serviceState = service.currentState as DbServiceStateData;
                // _this.logger && _this.logger.write(LL.DEBUG, "service State", serviceState, configService);
                if (!!serviceState.maxConnections && (configService.connectionAlertPercent < serviceState.currentConnections * 100 / serviceState.maxConnections)) {
                  service.isAlive = false;
                  service.lastProblem = Errors.CONNECTION_NUMBER_ALERT;
                }
              }
            }
          });
        }
      }
    }
  }

  //-- remove data that no longer has associated config
  for (let i = _this.healthCheckData.length - 1; i >= 0; i--) {
    let data = _this.healthCheckData[i];
    let toBeRemoved = false;
    //-- find associated config
    let config = _this.healthCheckConfig.find(c => c.domain === data.domain && c.group === data.group);
    if (!config) {
      toBeRemoved = true;
    }
    //-- only remove in case of FixedGroupConfig, AutoScale will have to make request to deregister
    if (config && config.config instanceof HealthCheckFixedGroupConfig) {
      let server = config.config.servers.find(s => s.serverName === data.serverName);
      if (!server) {
        toBeRemoved = true;
      } else {
        //-- match the service and remove service info if not found in config
        for (let j = data.services.length - 1; j >= 0; j++) {
          let service = data.services[j];
          let correspondingService = server.services.find(s => s.serviceName === service.serviceName);
          if (!correspondingService) {
            _this.logger && _this.logger.write(LL.VERBOSE, "Remove dangling service data from server data", service, data);
            data.services = [
              ...data.services.slice(0, j),
              ...data.services.slice(j + 1)
            ]
          }
        }
      }
    }
    if (toBeRemoved) {
      _this.logger && _this.logger.write(LL.VERBOSE, "Remove dangling health check server data", data);
      _this.healthCheckData = [
        ..._this.healthCheckData.slice(0, i),
        ..._this.healthCheckData.slice(i + 1),
      ]
    }
  }

};

const saveHealthCheckDataToRedis = async (_this: HealthCheckService) => {
  _this.logger && _this.logger.write(LL.VERBOSE, "Start caching data...");
  _this.cacheAdapter && await _this.cacheAdapter.setCache("HEALTH_CHECK_CONFIG", JSON.stringify(_this.healthCheckConfig));
  _this.cacheAdapter && await _this.cacheAdapter.setCache("HEALTH_CHECK_DATA", JSON.stringify(_this.healthCheckData));
  _this.logger && _this.logger.write(LL.VERBOSE, "Finish caching data.");
};
