import {ClaireError} from "claire-framework";

export class Errors {

    public static readonly READ_DIR_ERROR = new ClaireError("READ_DIR_ERROR");
    public static readonly XML_JSON_PARSE_ERROR = new ClaireError("XML_JSON_PARSE_ERROR");
    public static readonly INVALID_XSD_SCHEMA = new ClaireError("INVALID_XSD_SCHEMA");
    public static readonly NO_CORRESPONDING_CONFIG = new ClaireError("NO_CORRESPONDING_CONFIG");
    public static readonly SYSTEM_ERROR = new ClaireError("SYSTEM_ERROR");
    public static readonly CONNECTION_NUMBER_ALERT = new ClaireError("CONNECTION_NUMBER_ALERT");
    public static readonly LAST_CONTACT_THRESHOLD = new ClaireError("LAST_CONTACT_THRESHOLD");

}
