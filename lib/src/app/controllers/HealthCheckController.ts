import {
  HTTP,
  IAppContext,
  IController,
  IControllerHandler,
  ILogger,
  IResponse,
  LL, ResponseFailure,
  ResponseSuccess
} from "claire-framework";
import {HealthCheckService} from "../services/HealthCheckService";
import {convertHealthCheckDataFromPost} from "../types/converters/healthcheck";
import {Errors} from "../Errors";

export class HealthCheckController implements IController {

  private logger?: ILogger;
  private healthCheckService?: HealthCheckService;

  async init(appContext: IAppContext): Promise<IResponse> {
    this.logger = appContext.getLogger();
    this.healthCheckService = appContext.getServiceProvider().get(HealthCheckService) as HealthCheckService;
    return new ResponseSuccess();
  }

  getRoutePrefix(): string {
    return "/";
  }

  getRoutes(): Array<IControllerHandler> {
    return [
      [HTTP.GET, "/healthcheck", this.getStatus, []],
      [HTTP.GET, "/healthcheck/:domain/:group?", this.getHealthCheckData, []],
      [HTTP.POST, "/healthcheck", this.postHealthCheckData, []],
    ];
  }

  async getStatus(req: any, res: any): Promise<void> {
    res.json(new ResponseSuccess());
  }

  /*
    data = [{
      domain: string,
      group: string,
      serverName: string,
      services: Array<{
        serviceName: string,
        isAlive: boolean,
        lastContact: number,
        lastProblem: any,
        lastState: any,
      }>
    }]
   */
  async getHealthCheckData(req: any, res: any): Promise<void> {
    this.logger && this.logger.write(LL.VERBOSE, "Client request for health check data: ", req.params);

    const {domain, group} = req.params;
    const result = this.healthCheckService && await this.healthCheckService.getHealthCheckData(domain, group);
    if (!result) {
      return res.json(new ResponseFailure(Errors.SYSTEM_ERROR));
    }
    return res.json(new ResponseSuccess({servers: result.getResult()}));
  }

  /*
  body = {
    domain: string,
    group: string,
    name: string,
    services: Array<{
      name: string,
      status: {
        success: boolean,
        result: any
      }
      last_contact: number,
    }>
  }
   */
  async postHealthCheckData(req: any, res: any): Promise<void> {
    this.logger && this.logger.write(LL.VERBOSE, "Agent posting health check data", req.body);
    const healthCheckData = convertHealthCheckDataFromPost(req.body);
    this.logger && this.logger.write(LL.VERBOSE, "Data after convert", healthCheckData);

    this.logger && this.logger.write(LL.VERBOSE, "Appending health check data", JSON.stringify(healthCheckData));
    let result = this.healthCheckService && await this.healthCheckService.appendHealthCheckData(healthCheckData);
    this.logger && this.logger.write(LL.VERBOSE, "append result: ", result);
    return res.json(result);
  }
}